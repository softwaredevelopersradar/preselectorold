﻿namespace WorkPort
{
    public enum PreselectorNumber
    {
        AllPreselector,
        FirstPreselector,
        SecondPreselector
    }

    public enum FreqLO
    {
        Range860_1500MHz,
        Range470_880MHz,
        Range292_490MHz,
        Range186_340MHz,
        Range120_210MHz,
        Range75_131MHz,
        Range60_90MHz,
        Range30_70MHz
    }
    public enum FreqHI
    {
        Range1500_3000MHz,
        Range3000_4000MHz,
        Range4000_5000MHz,
        Range5000_6000MHz
    }
    public enum NumberPreampFilter
    {
        Range500_3000MHzAndUHF,
        Less500MHzAndUHF,
        Bypass,
        More3000MHzAndUHF
    }
    public enum FlagOnOff
    {
        OFF,
        ON
    }

    public enum LOorHI
    {
        HI,
        LO
    }

    public enum PowerPreamp
    {
        PreampOff,
        Preamp30_500M,
        Preamp500M_3G,
        Preamp3G_6G
    }

    public enum ExternalSwitchState
    {
        CH1 = 1,
        CH2 = 2,
        CH3 = 3,
        CH4 = 4,
        OFF = 5
    }

    public enum ModePreamp
    {
        AllRange,
        CurrentRange
    }

    public enum OnOffPreamp
    {
        ON,
        OFF
    }
    public enum SetPreampRange
    {
        Range30_70MHz,
        Range60_900MHz,
        Range75_131MHz,
        Range120_210MHz,
        Range186_340MHz,
        Range292_490MHz,
        Range470_880MHz,
        Range860_1500MHz,
        Range1500_3000MHz,
        Range3000_4000MHz,
        Range4000_5000MHz,
        Range5000_6000MHz
    }

    public enum RFChannel
    {
        AllRFChannelsOFF,
        FirstChannel,
        SecondChannel,
        ThirdChannel,
        FourthChannel
    }

    public enum OnOffSwitch
    {
        OFF,
        ON
    }

    public enum BaudRate
    {
        Baud110 = 110,
        Baud300 = 300,
        Baud600 = 600,
        Baud1200 = 1200,
        Baud2400 = 2400,
        Baud4800 = 4800,
        Baud9600 = 9600,
        Baud19200 = 19200,
        Baud38400 = 38400,
        Baud57600 = 57600,
        Baud115200 = 115200,
        Baud230400 = 230400
    }
}
