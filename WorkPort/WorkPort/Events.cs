﻿using System;

namespace WorkPort
{
    public class StateEventArgs : EventArgs
    {
        public byte GainLO1 { get; set; }
        public byte GainHI1 { get; set; }
        public byte GainPreamp1 { get; set; }
        public FreqLO StateFilterLO1 { get; set; }
        public FreqHI StateFilterHI1 { get; set; }
        public NumberPreampFilter NumberFilter1 { get; set; }
        public FlagOnOff FlagPreamp1 { get; set; }
        public LOorHI LOorHI1 { get; set; }
        public PowerPreamp PowerPreamp1 { get; set; }
        public LOorHI LOorHI2 { get; set; }
        public PowerPreamp PowerPreamp2 { get; set; }
        public byte GainLO2 { get; set; }
        public byte GainHI2 { get; set; }
        public byte GainPreamp2 { get; set; }
        public FreqLO StateFilterLO2 { get; set; }
        public FreqHI StateFilterHI2 { get; set; }
        public NumberPreampFilter NumberFilter2 { get; set; }
        public FlagOnOff FlagPreamp2 { get; set; }
        public ExternalSwitchState ExternalSwitch1 { get; set; }
        public ExternalSwitchState ExternalSwitch2 { get; set; }
        

        public StateEventArgs(byte GainLO1, byte GainHI1, byte GainPreamp1, FreqLO StateFilterLO1, FreqHI StateFilterHI1, NumberPreampFilter NumberFilter1, FlagOnOff FlagPreamp1, LOorHI LOorHI1, PowerPreamp PowerPreamp1,
            byte GainLO2, byte GainHI2, byte GainPreamp2, FreqLO StateFilterLO2, LOorHI LOorHI2, PowerPreamp PowerPreamp2, FreqHI StateFilterHI2, NumberPreampFilter NumberFilter2, FlagOnOff FlagPreamp2, ExternalSwitchState ExternalSwitch1, ExternalSwitchState ExternalSwitch2)
        {
            this.GainLO1 = GainLO1;
            this.GainHI1 = GainHI1;
            this.GainPreamp1 = GainPreamp1;
            this.StateFilterLO1 = StateFilterLO1;
            this.StateFilterHI1 = StateFilterHI1;
            this.NumberFilter1 = NumberFilter1;
            this.FlagPreamp1 = FlagPreamp1;
            this.LOorHI1 = LOorHI1;
            this.PowerPreamp1 = PowerPreamp1;
            this.LOorHI2 = LOorHI2;
            this.PowerPreamp2 = PowerPreamp2;
            this.GainLO2 = GainLO2;
            this.GainHI2 = GainHI2;
            this.GainPreamp2 = GainPreamp2;
            this.StateFilterLO2 = StateFilterLO2;
            this.StateFilterHI2 = StateFilterHI2;
            this.NumberFilter2 = NumberFilter2;
            this.FlagPreamp2 = FlagPreamp2;
            this.ExternalSwitch1 = ExternalSwitch1;
            this.ExternalSwitch2 = ExternalSwitch2;
        }

        public StateEventArgs()
        {
            GainLO1 = 0;
            GainHI1 = 0;
            GainPreamp1 = 0;
            StateFilterLO1 = 0;
            StateFilterHI1 = 0;
            NumberFilter1 = 0;
            FlagPreamp1 = 0;
            LOorHI1 = 0;
            PowerPreamp1 = 0;
            LOorHI2 = 0;
            PowerPreamp2 = 0;
            GainLO2 = 0;
            GainHI2 = 0;
            GainPreamp2 = 0;
            StateFilterLO2 = 0;
            StateFilterHI2 = 0;
            NumberFilter2 = 0;
            FlagPreamp2 = 0;
            ExternalSwitch1 = 0;
            ExternalSwitch2 = 0;
        }
    }

    public class FreqGainEventArgs : EventArgs
    {
        public short Freq { get; set; }
        public byte Gain { get; set; }

        public FreqGainEventArgs(short Freq, byte Gain)
        {
            this.Freq = Freq;
            this.Gain = Gain;
        }
    }
}
