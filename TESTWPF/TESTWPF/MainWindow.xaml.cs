﻿using Microsoft.Win32;
using System;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using WorkPort;

namespace TESTWPF
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private PreselModelOld p;

        private PreselTcpOld preselTcp;

        private PreselCOMOld preselCom;

        private bool typeConnection = false;
        private bool status = false;
        private string str = ""; private string LowByteFreq = ""; private string HighByteFreq = ""; private string sendFreq = "";  private string command = "";
        private void SendButton_Click(object sender, RoutedEventArgs e)
        {
            
            string message = "";
            message = SendBox.Text;
            try
            {
                p.SetCommand(message);
                SendBox.Clear();
            }
            catch (Exception)
            {
                SendBox.Clear();
            }
        }

        bool flag = false;
       

        private void AnswerDecryption(string message)
        {
            if (flag)
            {
                Display.AppendText("Расшифровка:\r\n" + message + "\r\n");
                flag = false;
                Display.ScrollToEnd();
            }
        }

        

        private void ConnectButton_Click(object sender, RoutedEventArgs e)
        {
            string temp;
            string temp1 = (string)PortNameBox.SelectedItem;
            temp = (string)BaudRateBox.SelectedItem;
            int temp2 = Convert.ToInt32(temp);
            this.preselCom = new PreselCOMOld(temp1, temp2);
            this.p = this.preselCom as PreselModelOld;
            try
            {
                status = false;
                status = p.Connect();
                if (status==true)
                {
                    CheckEl.Fill = new SolidColorBrush(Colors.PaleGreen);
                    this.preselCom.OnWrite += WriteMethod;
                    this.preselCom.OnRead += ReadMethod;
                    p.OnPreselectorNumber += P_OnPreselectorNumber;
                    p.OnState_00 += P_OnState;
                    p.OnFreq_01 += P_OnFreq01;
                    p.OnGain_02 += P_OnGain02;
                    p.OnModePreamp_03 += P_OnModePreamp;
                    p.OnOnOffPreamp_04 += P_OnOnOffPreamp;
                    p.OnSetPreampRange_05 += P_OnSetPreampRange;
                    p.OnSetFreqGain_06 += P_OnSetFreqGain_06;
                    p.OnModeExternalSwitches_07 += P_OnModeExternalSwitches;
                    p.OnRFChannel_08 += P_OnRFChannel;
                    p.OnGain_09 += P_OnGain09;
                    p.OnAttenuatorLevelLow_0A += P_OnAttenuatorLevelLow;
                    p.OnAttenuatorLevelHigh_0B += P_OnAttenuatorLevelHigh;
                    p.OnOnOffSwitch_0F += P_OnOnOffSwitch;
                    p.OnOnOffOpticalTransmitter_10 += P_OnOnOffOpticalTransmitter;
                    p.OnModeAttenuator_11 += P_OnModeAttenuator;
                    p.OnConfigurationChanels_12 += P_OnConfigurationChanels;
                    p.OnSave_13 += P_OnSave;
                    p.OnReset_14 += P_OnReset;
                    p.OnGain_15 += P_OnGain_15;
                }
                else
                {
                    throw new Exception("Port isn`t open. \r\n");
                }
                ConnectButton.IsEnabled = false;
                DisconnectButton.IsEnabled = true;
                SendButton.IsEnabled = true;
                PortNameBox.IsEnabled = false;
                BaudRateBox.IsEnabled = false;
            }
            catch
            {
                 throw new Exception("Port isn`t open. \r\n");
            }
        }

        private void P_OnGain_15(object sender, byte e)
        {
            Dispatcher.Invoke(() =>
            {
                Display.AppendText($"Установка номинального усиления тракта: {e} дБ\r\n");
            });
        }

        private void P_OnReset(object sender, bool e)
        {
            Dispatcher.Invoke(() =>
            {
                Display.AppendText($"Сброс настроек на заводские\r\n");
            });
        }

        private void P_OnSave(object sender, bool e)
        {
            Dispatcher.Invoke(() =>
            {
                Display.AppendText($"Сохранение параметров текущей полосы\r\n");
            });
        }

        private void P_OnConfigurationChanels(object sender, short[] e)
        {
            Dispatcher.Invoke(() =>
            {
                Display.AppendText($"Конфигурирование каналов для системы коммутации на внешних коммутаторах ZSWA4-63DRB+: {e[0]} {e[1]} {e[2]} {e[3]}\r\n");
            });
        }

        private void P_OnModeAttenuator(object sender, ModePreamp e)
        {
            Dispatcher.Invoke(() =>
            {
                Display.AppendText($"Установка режима работы аттенюатора в канале предусилителя: {e}\r\n");
            });
        }

        private void P_OnOnOffOpticalTransmitter(object sender, OnOffSwitch e)
        {
            Dispatcher.Invoke(() =>
            {
                Display.AppendText($"Питание 12В для оптического передатчика или питание 5В для дополнительного РЧ-усилителя: {e}\r\n");
            });
        }

        private void P_OnOnOffSwitch(object sender, OnOffSwitch e)
        {
            Dispatcher.Invoke(() =>
            {
                Display.AppendText($"Питание 3.3В для внешних коммутаторов ZSWA4-63DRB+: {e}\r\n");
            });
        }

        private void P_OnAttenuatorLevelHigh(object sender, byte e)
        {
            Dispatcher.Invoke(() =>
            {
                Display.AppendText($"Уровень аттенюатора в верхнем диапазоне (1500..6000 MHz): {e} дБ\r\n");
            });
        }

        private void P_OnAttenuatorLevelLow(object sender, byte e)
        {
            Dispatcher.Invoke(() =>
            {
                Display.AppendText($"Уровень аттенюатора в нижнем диапазоне (30..1500MHz): {e} дБ\r\n");
            });
        }

        private void P_OnGain09(object sender, byte e)
        {
            Dispatcher.Invoke(() =>
            {
                Display.AppendText($"Уровень аттенюатора предусилителя для 2-ух каналов: {e} дБ\r\n");
            });
        }

        private void P_OnSetFreqGain_06(object sender, FreqGainEventArgs e)
        {
            Dispatcher.Invoke(() =>
            {
                Display.AppendText($"Установка частоты: {e.Freq} МГц\r\n");
                Display.AppendText($"Установка усиления: {e.Gain} дБ\r\n");
            });
        }

        private void P_OnRFChannel(object sender, RFChannel e)
        {
            Dispatcher.Invoke(() =>
            {
                Display.AppendText($"Управление системой коммутации с внешними коммутаторами ZSWA4-63DRB+: {e}\r\n");
            });
        }

        private void P_OnModeExternalSwitches(object sender, ModePreamp e)
        {
            Dispatcher.Invoke(() =>
            {
                Display.AppendText($"Установка режима работы внешних коммутаторов ZSWA4-63DRB+: {e}\r\n");
            });
        }

        private void P_OnSetPreampRange(object sender, SetPreampRange e)
        {
            Dispatcher.Invoke(() =>
            {
                Display.AppendText($"Настройка преселектора по номеру диапазона: {e}\r\n");
            });
        }

        private void P_OnOnOffPreamp(object sender, OnOffPreamp e)
        {
            Dispatcher.Invoke(() =>
            {
                Display.AppendText($"Включение/выключение предусилителя: {e}\r\n");
            });
        }

        private void P_OnModePreamp(object sender, ModePreamp e)
        {
            Dispatcher.Invoke(() =>
            {
                Display.AppendText($"Режима работы предусилителя: {e}\r\n");
            });
        }

        private void P_OnGain02(object sender, byte e)
        {
            Dispatcher.Invoke(() =>
            {
                Display.AppendText($"Установлено усиление для 2-ух каналов: {e} дБ\r\n");
            });
        }

        private void P_OnFreq01(object sender, short e)
        {
            Dispatcher.Invoke(() =>
            {
                Display.AppendText($"Установлена частота: {e} МГц\r\n");
            });
        }

        private void P_OnPreselectorNumber(object sender, PreselectorNumber e)
        {
            Dispatcher.Invoke(() =>
            {
                Display.AppendText($"Aдрес устройства, к которому идет обращение: {e}\r\n");
            });
        }

        private void P_OnState(object sender, StateEventArgs e)
        {
            Dispatcher.Invoke(() =>
            {
                Display.AppendText($"Значение аттенюатора LO 1-го канала: {e.GainLO1} дБ\r\n");
                Display.AppendText($"Значение аттенюатора HI 1-го канала: {e.GainHI1} дБ\r\n");
                Display.AppendText($"Значение аттенюатора предусилителя 1-го канала: {e.GainPreamp1} дБ\r\n");
                Display.AppendText($"Номер фильтра LO 1-го канала: {e.StateFilterLO1}\r\n");
                Display.AppendText($"Номер фильтра HI 1-го канала: {e.StateFilterHI1}\r\n");
                Display.AppendText($"Hомер фильтра предусилителя 1-го канала: {e.NumberFilter1}\r\n");
                Display.AppendText($"Флаг предусилителя в 1-м канале: {e.FlagPreamp1}\r\n");
                Display.AppendText($"Bыбор тракта LO/HI 1-го канала: {e.LOorHI1}\r\n");
                Display.AppendText($"Питание предусилителя 1-го канала: {e.PowerPreamp1}\r\n");
                Display.AppendText($"Bыбор тракта LO/HI 1-го канала: {e.LOorHI2}\r\n");
                Display.AppendText($"Питание предусилителя 1-го канала: {e.PowerPreamp2}\r\n");
                Display.AppendText($"Значение аттенюатора LO 2-го канала: {e.GainLO2} дБ\r\n");
                Display.AppendText($"Значение аттенюатора HI 2-го канала: {e.GainHI2} дБ\r\n");
                Display.AppendText($"Значение аттенюатора предусилителя 2-го канала: {e.GainPreamp2} дБ\r\n");
                Display.AppendText($"Номер фильтра LO 2-го канала: {e.StateFilterLO2}\r\n");
                Display.AppendText($"Номер фильтра HI 2-го канала: {e.StateFilterHI2}\r\n");
                Display.AppendText($"Hомер фильтра предусилителя 2-го канала: {e.NumberFilter2}\r\n");
                Display.AppendText($"Флаг предусилителя в 2-м канале: {e.FlagPreamp2}\r\n");
                Display.AppendText($"Cостояние внешнего коммутатора 1: {e.ExternalSwitch1}\r\n");
                Display.AppendText($"Cостояние внешнего коммутатора 2: {e.ExternalSwitch2}\r\n");
                Display.ScrollToEnd();
            });
        }

        private void WriteMethod(object sender, string e)
        {
            e = AddSpaces(e);
            command = e;
            Display.AppendText($"Команда: {e}\r\n");
        }

        private void ReadMethod(object sender, string e)
        {
            Dispatcher.Invoke(() => { Display.AppendText($"Ответ: {e}\r\n"); });
        }

        private void DisconnectButton_Click(object sender, RoutedEventArgs e)
         {
            status = p.Disconnect();

            if (status == false)
            {
                CheckEl.Fill = new SolidColorBrush(Colors.Red);
                ConnectButton.IsEnabled = true;
                DisconnectButton.IsEnabled = false;
                SendButton.IsEnabled = false;
                PortNameBox.IsEnabled = true;
                BaudRateBox.IsEnabled = true;
            }
        }

        
        private void Test_COM_Ports_Loaded(object sender, RoutedEventArgs e)
        {
            SetPortName("COM");
            SetPortParity(Parity.Even);
            SetPortBaudRate(9600);
            SetPortStopBits(StopBits.None);
            BaudRateget();
            AddToCombo(); AddAddressDevice();AddGain();AddPreamp(); AddOnOffPreamp(); AddPreselectorSetting(); AddOperationMode(); AddLink();
            CheckEl.Fill = new SolidColorBrush(Colors.Red);
            ConnectButton.IsEnabled = true;
            DisconnectButton.IsEnabled = false;
            SendButton.IsEnabled = false;
            FreqBox.MaxLength = 4;
            LowAndHighByte.MaxLength = 4;
        } 
        
        public string SetPortName(string defaultPortName)
        {
            string portname = "";
            foreach (string PortName in SerialPort.GetPortNames())
            {
                PortNameBox.Items.Add(PortName);
            }
            PortNameBox.SelectedIndex = 0;
            if (portname == "")
            {
                portname = defaultPortName;
            }
            return portname;
        }

        public int SetPortBaudRate (int defaultPortBaudRate)
        {
            string baudRate = "";
            if (baudRate == "")
            {
                baudRate = defaultPortBaudRate.ToString();
            }
            return int.Parse(baudRate);
        }

        public Parity SetPortParity (Parity defaultPortParity)
        {
            string parity = "";
            foreach (string paritybit in Enum.GetNames(typeof(Parity)))
            {
                ParityBox.Items.Add(paritybit);
            }
            ParityBox.SelectedIndex = 0;
            if (parity == "")
            {
                parity = defaultPortParity.ToString();
            }
            return (Parity)Enum.Parse(typeof(Parity), parity);
        }

        public StopBits SetPortStopBits (StopBits defaultPortStopBits)
        {
            string stopBits = "";
            foreach(string stopbits in Enum.GetNames(typeof(StopBits)))
            {
                stopBits = stopbits;
                StopBitsBox.Items.Add(stopbits);    
            }
            StopBitsBox.SelectedIndex = 1;
            if(stopBits == "")
            {
                stopBits = defaultPortStopBits.ToString();
            }
            return (StopBits)Enum.Parse(typeof(StopBits), stopBits);
        }

        public void BaudRateget()
        {
            string[] listbaudrate = new string[15];
            listbaudrate[0] = "110";
            listbaudrate[1] = "300";
            listbaudrate[2] = "600";
            listbaudrate[3] = "1200";
            listbaudrate[4] = "2400";
            listbaudrate[5] = "4800";
            listbaudrate[6] = "9600";
            listbaudrate[7] = "14400";
            listbaudrate[8] = "19200";
            listbaudrate[9] = "38400";
            listbaudrate[10] = "56000";
            listbaudrate[11] = "57600";
            listbaudrate[12] = "115200";
            listbaudrate[13] = "128000";
            listbaudrate[14] = "256000";
            foreach (string baudRate in listbaudrate)
            {
                BaudRateBox.Items.Add(baudRate);
            }
            BaudRateBox.SelectedIndex = 12;
        }

        private void SendBox_TextChanged(object sender, TextChangedEventArgs e)
       {
           TextBox tb = sender as TextBox;
           string text = tb.Text.ToUpper();
           char[] symbols = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', ' '};
           tb.Text = new string(text.Where(x => symbols.Contains(x)).ToArray());
           SendBox.SelectionStart = SendBox.Text.Length;
       }

        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            Display.Clear();
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFile = new SaveFileDialog();
            saveFile.Filter = "Text file(*.txt)|*.txt|c# file (*.cs)|*.cs";
            saveFile.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            if (saveFile.ShowDialog() == true)
            {
                File.WriteAllText(saveFile.FileName, Display.Text);
            }
        }
        
        private void AddAddressDevice()
        {
            foreach(string Device in AddDeviceAddressCombo())
            {
                DeviceAddressBox.Items.Add(Device);
            }
            DeviceAddressBox.SelectedIndex = 0;
        }

        private void AddPreselectorSetting()
        {
            foreach (string Preselector in AddRengFreqCombo())
            {
                PreselectorSetting.Items.Add(Preselector);
            }
            PreselectorSetting.SelectedIndex = 0;
        }


        private void AddGain()
        {
            foreach (string Gain in AddGainCombo())
            {
                GainBox.Items.Add(Gain);
                GainBox1.Items.Add(Gain);
                GainBox2.Items.Add(Gain);
                GainBox3.Items.Add(Gain);
                GainBox4.Items.Add(Gain);
            }
            GainBox.SelectedIndex = 0;
            GainBox1.SelectedIndex = 0;
            GainBox2.SelectedIndex = 0;
            GainBox3.SelectedIndex = 0;
            GainBox4.SelectedIndex = 0;
        }

        private void AddPreamp()
        {
            foreach (string Preamp in AddPreampCombo())
            {
                PreampBox.Items.Add(Preamp);
            }
            PreampBox.SelectedIndex = 0;
        }

        private void AddOnOffPreamp()
        {
            foreach (string Preamp in OnOffPreampCombo())
            {
                OnOffPreamp1.Items.Add(Preamp);
                OnOffPreamp2.Items.Add(Preamp);
            }
            foreach (string Preamp in OnOffPreampCombo1())
            {
                OnOffPreamp.Items.Add(Preamp);
            }
            OnOffPreamp.SelectedIndex = 0;
            OnOffPreamp1.SelectedIndex = 0;
            OnOffPreamp2.SelectedIndex = 0;
        }

        private void AddOperationMode()
        {
            foreach (string Operation in AddOperationModCombo())
            {
                RangeBox.Items.Add(Operation);
                RangeBox1.Items.Add(Operation);
            }
            RangeBox.SelectedIndex = 0;
            RangeBox1.SelectedIndex = 0;
        }

        private void AddLink()
        {
            foreach (string Link in AddLinkCombo())
            {
                LinkBox.Items.Add(Link);
            }
            LinkBox.SelectedIndex = 0;
        }

        private void SendStatus_Click(object sender, RoutedEventArgs e)
        {
            p.GetStatePreselectorCode0();
        }

        
        private void SendFreq_Click(object sender, RoutedEventArgs e)
        {
            string message = ""; string hex = "";
            message = FreqBox.Text;
            int value = Convert.ToInt32(message);
            if (value < 30) { message = "30"; }
            else if (value > 6000) { message = "6000"; }
            value = Convert.ToInt32(message);
            hex = value.ToString("X");
            sendFreq = message;
            if (hex.Length != 0)
            {
                if (hex.Length == 2) { LowByteFreq = hex; HighByteFreq = "00"; }
                else if (hex.Length == 3)
                {
                    string str = hex.Substring(1, 2);
                    LowByteFreq = str;
                    str = hex.Substring(0, 1);
                    HighByteFreq = $"0{str}";
                }
                else if (hex.Length == 4)
                {
                    string str = hex.Substring(1, 2);
                    LowByteFreq = str;
                    str = hex.Substring(0, 2);
                    HighByteFreq = str;
                }
            }
            int DeviceCount = Convert.ToInt32(DeviceAddressBox.SelectedIndex);
            string[] DeviceMas = AddValueDeviceAddressCombo();
            string DeviceAddress = DeviceMas[DeviceCount];
            p.SetFreqCode1((short)value, (PreselectorNumber)Convert.ToByte(DeviceAddress));
        }

        private void SendGain_Click(object sender, RoutedEventArgs e)
        {
            int count = Convert.ToInt32(GainBox.SelectedIndex);
            string[] mas = AddValueGainCombo();
            string gain = mas[count];
            int DeviceCount = Convert.ToInt32(DeviceAddressBox.SelectedIndex);
            string[] DeviceMas = AddValueDeviceAddressCombo();
            string DeviceAddress = DeviceMas[DeviceCount];
            p.SetGainCode2((byte)Convert.ToInt16(gain, 16), (PreselectorNumber)Convert.ToByte(DeviceAddress));
        }

        private void PreampSend_Click(object sender, RoutedEventArgs e)
        {
            int count = Convert.ToInt32(PreampBox.SelectedIndex);
            string[] mas = AddParamPreampCombo();
            string ParamPreamp = mas[count];
            int DeviceCount = Convert.ToInt32(DeviceAddressBox.SelectedIndex);
            string[] DeviceMas = AddValueDeviceAddressCombo();
            string DeviceAddress = DeviceMas[DeviceCount];
            p.SetPreampCode3((ModePreamp)((byte)Convert.ToInt16(ParamPreamp)), (PreselectorNumber)Convert.ToByte(DeviceAddress));
        }

        private void OnOffSend_Click(object sender, RoutedEventArgs e)
        {
            int count = Convert.ToInt32(OnOffPreamp.SelectedIndex);
            string[] mas = AddParamPreampCombo();
            string ParamPreamp = mas[count];
            int DeviceCount = Convert.ToInt32(DeviceAddressBox.SelectedIndex);
            string[] DeviceMas = AddValueDeviceAddressCombo();
            string DeviceAddress = DeviceMas[DeviceCount];
            p.SetOnOffCode4((FlagOnOff)Convert.ToByte(ParamPreamp), (PreselectorNumber)Convert.ToByte(DeviceAddress));
        }

        private void SelectorButton_Click(object sender, RoutedEventArgs e)
        {
            int count = Convert.ToInt32(PreselectorSetting.SelectedIndex);
            string[] mas = AddValueFreqCombo();
            string Preselector = mas[count];
            int DeviceCount = Convert.ToInt32(DeviceAddressBox.SelectedIndex);
            string[] DeviceMas = AddValueDeviceAddressCombo();
            string DeviceAddress = DeviceMas[DeviceCount];
            p.SetPreselectorSettingCode5((SetPreampRange)Convert.ToByte(Preselector, 16), (PreselectorNumber)Convert.ToByte(DeviceAddress));
        }

        private void FreqGain_Click(object sender, RoutedEventArgs e)
        {
            int count = Convert.ToInt32(GainBox1.SelectedIndex);
            string[] mas = AddValueGainCombo();
            string Gain = mas[count];
            string message = ""; string hex = "";
            message = LowAndHighByte.Text;
            int value = Convert.ToInt32(message);
            if (value < 30) { message = "30"; }
            else if (value > 6000) { message = "6000"; }
            value = Convert.ToInt32(message);
            hex = value.ToString("X");
            sendFreq = message;
            if (hex.Length != 0)
            {
                if (hex.Length == 2) { LowByteFreq = hex; HighByteFreq = "00"; }
                else if (hex.Length == 3)
                {
                    string str = hex.Substring(1, 2);
                    LowByteFreq = str;
                    str = hex.Substring(0, 1);
                    HighByteFreq = $"0{str}";
                }
                else if (hex.Length == 4)
                {
                    string str = hex.Substring(1, 2);
                    LowByteFreq = str;
                    str = hex.Substring(0, 2);
                    HighByteFreq = str;
                }
                int DeviceCount = Convert.ToInt32(DeviceAddressBox.SelectedIndex);
                string[] DeviceMas = AddValueDeviceAddressCombo();
                string DeviceAddress = DeviceMas[DeviceCount];
                p.SetFreqGainCode6((short)value, (byte)Convert.ToInt16(Gain, 16), (PreselectorNumber)Convert.ToByte(DeviceAddress));
            }
            else { Display.AppendText($"Введите команду!\r\n"); }
        }

        private void SettingOperationMode_Click(object sender, RoutedEventArgs e)
        {
            int count = Convert.ToInt32(RangeBox.SelectedIndex);
            string[] mas = AddValueOperationModCombo();
            string Operation = mas[count];
            int DeviceCount = Convert.ToInt32(DeviceAddressBox.SelectedIndex);
            string[] DeviceMas = AddValueDeviceAddressCombo();
            string DeviceAddress = DeviceMas[DeviceCount];
            p.SetOperationModCode7((ModePreamp)Convert.ToByte(Operation), (PreselectorNumber)Convert.ToByte(DeviceAddress));
        }

        private void LinkButton_Click(object sender, RoutedEventArgs e)
        {
            int count = Convert.ToInt32(LinkBox.SelectedIndex);
            string[] mas = AddValueLinkCombo();
            string Link = mas[count];
            int DeviceCount = Convert.ToInt32(DeviceAddressBox.SelectedIndex);
            string[] DeviceMas = AddValueDeviceAddressCombo();
            string DeviceAddress = DeviceMas[DeviceCount];
            p.SetLinkCode8((RFChannel)Convert.ToByte(Link), (PreselectorNumber)Convert.ToByte(DeviceAddress));
        }

        private void AttenuatorLevelButton_Click(object sender, RoutedEventArgs e)
        {
            int count = Convert.ToInt32(GainBox2.SelectedIndex);
            string[] mas = AddValueGainCombo();
            string Gain = mas[count];
            int DeviceCount = Convert.ToInt32(DeviceAddressBox.SelectedIndex);
            string[] DeviceMas = AddValueDeviceAddressCombo();
            string DeviceAddress = DeviceMas[DeviceCount];
            p.SetAttenuatorLevelCode9((byte)Convert.ToInt16(Gain, 16), (PreselectorNumber)Convert.ToByte(DeviceAddress));
        }

        private void AttenuatorLevelLowRangeButton_Click(object sender, RoutedEventArgs e)
        {
            int count = Convert.ToInt32(GainBox3.SelectedIndex);
            string[] mas = AddValueGainCombo();
            string Gain = mas[count];
            int DeviceCount = Convert.ToInt32(DeviceAddressBox.SelectedIndex);
            string[] DeviceMas = AddValueDeviceAddressCombo();
            string DeviceAddress = DeviceMas[DeviceCount];
            p.SetAttenuatorLowLevelCodeA((byte)Convert.ToInt16(Gain, 16), (PreselectorNumber)Convert.ToByte(DeviceAddress));
        }

        private void AttenuatorLevelHighRangeButton_Click(object sender, RoutedEventArgs e)
        {
            int count = Convert.ToInt32(GainBox4.SelectedIndex);
            string[] mas = AddValueGainCombo();
            string Gain = mas[count];
            int DeviceCount = Convert.ToInt32(DeviceAddressBox.SelectedIndex);
            string[] DeviceMas = AddValueDeviceAddressCombo();
            string DeviceAddress = DeviceMas[DeviceCount];
            p.SetAttenuatorHighLevelCodeB((byte)Convert.ToInt16(Gain, 16), (PreselectorNumber)Convert.ToByte(DeviceAddress));
        }

        private void PowerButton_Click(object sender, RoutedEventArgs e)
        {
            int count = Convert.ToInt32(OnOffPreamp1.SelectedIndex);
            string[] mas = AddParamPreampCombo();
            string Power = mas[count];
            int DeviceCount = Convert.ToInt32(DeviceAddressBox.SelectedIndex);
            string[] DeviceMas = AddValueDeviceAddressCombo();
            string DeviceAddress = DeviceMas[DeviceCount];
            p.SetPowerCodeF((OnOffSwitch)Convert.ToByte(Power), (PreselectorNumber)Convert.ToByte(DeviceAddress));
        }

        private void OpticalTranssmitterPower_Click(object sender, RoutedEventArgs e)
        {
            int count = Convert.ToInt32(OnOffPreamp2.SelectedIndex);
            string[] mas = AddParamPreampCombo();
            string Power = mas[count];
            int DeviceCount = Convert.ToInt32(DeviceAddressBox.SelectedIndex);
            string[] DeviceMas = AddValueDeviceAddressCombo();
            string DeviceAddress = DeviceMas[DeviceCount];
            p.SetOpticalPowerCode10((OnOffSwitch)Convert.ToByte(Power), (PreselectorNumber)Convert.ToByte(DeviceAddress));
        }

        private void SettingGainOperattionMode_Click(object sender, RoutedEventArgs e)
        {
            int count = Convert.ToInt32(RangeBox1.SelectedIndex);
            string[] mas = AddValueOperationModCombo();
            string Range = mas[count];
            int DeviceCount = Convert.ToInt32(DeviceAddressBox.SelectedIndex);
            string[] DeviceMas = AddValueDeviceAddressCombo();
            string DeviceAddress = DeviceMas[DeviceCount];
            p.SetGainOperationModeCode11((ModePreamp)Convert.ToByte(Range), (PreselectorNumber)Convert.ToByte(DeviceAddress));
        }
        private void SendSet_Click(object sender, RoutedEventArgs e)
        {
            string message = "";
            if (Box1.Text == Box3.Text || Box3.Text == Box5.Text || Box5.Text == Box7.Text || Box2.Text == Box4.Text || Box4.Text == Box6.Text || Box6.Text == Box8.Text)
            { message = "41322314"; }
            else { message = $"{Box1.Text}{Box2.Text}{Box3.Text}{Box4.Text}{Box5.Text}{Box6.Text}{Box7.Text}{Box8.Text}"; }
            int DeviceCount = Convert.ToInt32(DeviceAddressBox.SelectedIndex);
            string[] DeviceMas = AddValueDeviceAddressCombo();
            string DeviceAddress = DeviceMas[DeviceCount];
            int byteChannel = Convert.ToInt32(message);
            p.SetExternalSwitchCode12(message, (PreselectorNumber)Convert.ToByte(DeviceAddress));
        }

        private void ResetButton_Click(object sender, RoutedEventArgs e)
        {
            p.ResetCode14();
        }

        private void SaveButton1_Click(object sender, RoutedEventArgs e)
        {
            p.SetSaveCode13();
        }

        private string AddSpaces(string message)
        {
            int step = 2;
            string[] messageMass = message.Split(' ');
            message = string.Join("", messageMass);
            StringBuilder sb = new StringBuilder();
            sb.Append(message.Substring(0, step));
            for (int i = step; i < message.Length; i += step)
            {
                sb.Append(" ");
                for (int j = i; j < i + step && j < message.Length; j++)
                {
                    sb.Append(message[j]);
                }
            }
            message = sb.ToString();
            return message;
        }

        private void AddToCombo()
        {
            string[] mas = { "1", "2", "3", "4" };
            for (int k = 0; k < 4; k++)
            {
                Box1.Items.Add(mas[k]);
                Box2.Items.Add(mas[k]);
                Box3.Items.Add(mas[k]);
                Box4.Items.Add(mas[k]);
                Box5.Items.Add(mas[k]);
                Box6.Items.Add(mas[k]);
                Box7.Items.Add(mas[k]);
                Box8.Items.Add(mas[k]);
            }

            TypeConnectionComboBox.Items.Add("TCP");
            TypeConnectionComboBox.Items.Add("Com");
            TypeConnectionComboBox.SelectedIndex = 0;
        }
        private string[] AddValueFreqCombo()
        {
            string[] listValueFreq = new string[12];
            listValueFreq[0] = "00";
            listValueFreq[1] = "01";
            listValueFreq[2] = "02";
            listValueFreq[3] = "03";
            listValueFreq[4] = "04";
            listValueFreq[5] = "05";
            listValueFreq[6] = "06";
            listValueFreq[7] = "07";
            listValueFreq[8] = "08";
            listValueFreq[9] = "09";
            listValueFreq[10] = "0A";
            listValueFreq[11] = "0B";
            return listValueFreq;
        }

        private string[] AddRengFreqCombo()
        {
            string[] listRengFreq = new string[12];
            listRengFreq[0] = "30-70 МГц";
            listRengFreq[1] = "60-90 МГц";
            listRengFreq[2] = "75-131 МГц";
            listRengFreq[3] = "120-210 МГц";
            listRengFreq[4] = "186-340 МГц";
            listRengFreq[5] = "292-490 МГц";
            listRengFreq[6] = "470-880 МГц";
            listRengFreq[7] = "860-1500 МГц";
            listRengFreq[8] = "1500-3000 МГц";
            listRengFreq[9] = "3000-4000 МГц";
            listRengFreq[10] = "4000-5000 МГц";
            listRengFreq[11] = "5000-6000 МГц";
            return listRengFreq;
        }

        private string[] AddValueGainCombo()
        {
            string[] listGain = new string[32];
            listGain[0] = "00";
            listGain[1] = "01";
            listGain[2] = "02";
            listGain[3] = "03";
            listGain[4] = "04";
            listGain[5] = "05";
            listGain[6] = "06";
            listGain[7] = "07";
            listGain[8] = "08";
            listGain[9] = "09";
            listGain[10] = "0A";
            listGain[11] = "0B";
            listGain[12] = "0C";
            listGain[13] = "0D";
            listGain[14] = "0E";
            listGain[15] = "0F";
            listGain[16] = "10";
            listGain[17] = "11";
            listGain[18] = "12";
            listGain[19] = "13";
            listGain[20] = "14";
            listGain[21] = "15";
            listGain[22] = "16";
            listGain[23] = "17";
            listGain[24] = "18";
            listGain[25] = "19";
            listGain[26] = "1A";
            listGain[27] = "1B";
            listGain[28] = "1C";
            listGain[29] = "1D";
            listGain[30] = "1E";
            listGain[31] = "1F";
            return listGain;
        }

        private string[] AddGainCombo()
        {
            string[] listGain = new string[32];
            listGain[0] = "0 дБ";
            listGain[1] = "1 дБ";
            listGain[2] = "2 дБ";
            listGain[3] = "3 дБ";
            listGain[4] = "4 дБ";
            listGain[5] = "5 дБ";
            listGain[6] = "6 дБ";
            listGain[7] = "7 дБ";
            listGain[8] = "8 дБ";
            listGain[9] = "9 дБ";
            listGain[10] = "10 дБ";
            listGain[11] = "11 дБ";
            listGain[12] = "12 дБ";
            listGain[13] = "13 дБ";
            listGain[14] = "14 дБ";
            listGain[15] = "15 дБ";
            listGain[16] = "16 дБ";
            listGain[17] = "17 дБ";
            listGain[18] = "18 дБ";
            listGain[19] = "19 дБ";
            listGain[20] = "20 дБ";
            listGain[21] = "21 дБ";
            listGain[22] = "22 дБ";
            listGain[23] = "23 дБ";
            listGain[24] = "24 дБ";
            listGain[25] = "25 дБ";
            listGain[26] = "26 дБ";
            listGain[27] = "27 дБ";
            listGain[28] = "28 дБ";
            listGain[29] = "29 дБ";
            listGain[30] = "30 дБ";
            listGain[31] = "31 дБ";
            return listGain;
        }

        private string[] AddValueDeviceAddressCombo()
        {
            string[] listValueDevice = new string[3];
            listValueDevice[0] = "00";
            listValueDevice[1] = "01";
            listValueDevice[2] = "02";
            return listValueDevice;
        }

        private string[] AddDeviceAddressCombo()
        {
            string[] listDevice = new string[3];
            listDevice[0] = "Синхронный";
            listDevice[1] = "1-ый модуль";
            listDevice[2] = "2-ой модуль";
            return listDevice;
        }

        private string[] AddParamPreampCombo()
        {
            string[] listParamPreamp = new string[2];
            listParamPreamp[0] = "00";
            listParamPreamp[1] = "01";
            return listParamPreamp;
        }

        private string[] AddPreampCombo()
        {
            string[] listParamPreamp = new string[2];
            listParamPreamp[0] = "Для всех диапозонов";
            listParamPreamp[1] = "Для текущего диапозона";
            return listParamPreamp;
        }

        private string[] OnOffPreampCombo()
        {
            string[] listPreamp = new string[2];
            listPreamp[0] = "Выключить";
            listPreamp[1] = "Включить";
            return listPreamp;
        }

        private string[] OnOffPreampCombo1()
        {
            string[] listPreamp = new string[2];
            listPreamp[0] = "Включить";
            listPreamp[1] = "Выключить";
            return listPreamp;
        }
        private string[] AddValueOperationModCombo()
        {
            string[] listValueOperationMode = new string[2];
            listValueOperationMode[0] = "00";
            listValueOperationMode[1] = "01";
            return listValueOperationMode;
        }

        private string[] AddOperationModCombo()
        {
            string[] listOperationMode = new string[2];
            listOperationMode[0] = "Для всех";
            listOperationMode[1] = "Для каждого";
            return listOperationMode;
        }

        private string[] AddValueLinkCombo()
        {
            string[] listValueLink = new string[5];
            listValueLink[0] = "00";
            listValueLink[1] = "01";
            listValueLink[2] = "02";
            listValueLink[3] = "03";
            listValueLink[4] = "04";
            return listValueLink;
        }

        private string[] AddLinkCombo()
        {
            string[] listLink = new string[5];
            listLink[0] = "Для всех выключен";
            listLink[1] = "Для 1-ого РЧ-канала";
            listLink[2] = "Для 2-ого РЧ-канала";
            listLink[3] = "Для 3-ого РЧ-канала";
            listLink[4] = "Для 4-ого РЧ-канала";
            return listLink;
        }

        private void LowAndHighByte_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox tb = sender as TextBox;
            string text = tb.Text.ToUpper();
            char[] symbols = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
            tb.Text = new string(text.Where(x => symbols.Contains(x)).ToArray());
            LowAndHighByte.SelectionStart = LowAndHighByte.Text.Length;
        }

       
       
       

        private void SetFiltersFirstCanale(string state)
        {
            string binary = String.Join(String.Empty, state.Select
                (c => Convert.ToString(Convert.ToInt32(c.ToString(), 16), 2).PadLeft(4, '0')));
            string bite = binary.Substring(0, 3);
            Display.AppendText($"Фильтр LO 1-го канала:\r\n");
            if (bite == "000") { Display.AppendText("Дипазон 860-1500 МГц\r\n"); }
            else if (bite == "001") { Display.AppendText("Дипазон 470-880 МГц\r\n"); }
            else if (bite == "010") { Display.AppendText("Дипазон 292-490 МГц\r\n"); }
            else if (bite == "011") { Display.AppendText("Дипазон 186-340 МГц\r\n"); }
            else if (bite == "100") { Display.AppendText("Дипазон 120-210 МГц\r\n"); }
            else if (bite == "101") { Display.AppendText("Дипазон 75-131 МГц\r\n"); }
            else if (bite == "110") { Display.AppendText("Дипазон 60-90 МГц\r\n"); }
            else if (bite == "111") { Display.AppendText("Дипазон 30-70 МГц\r\n"); }
            bite = binary.Substring(3, 2);
            Display.AppendText($"Фильтр HI 1-го канала:\r\n");
            if (bite == "00") { Display.AppendText("Дипазон 1500-3000 МГц\r\n"); }
            else if (bite == "01") { Display.AppendText("Дипазон 3000-4000 МГц\r\n"); }
            else if (bite == "10") { Display.AppendText("Дипазон 4000-5000 МГц\r\n"); }
            else if (bite == "11") { Display.AppendText("Дипазон 5000-6000 МГц\r\n"); }
            bite = binary.Substring(5, 2);
            Display.AppendText($"Фильтр предусилителя 1-го канала:\r\n");
            if (bite == "00") { Display.AppendText("500-3000 МГц + УВЧ\r\n"); }
            else if (bite == "01") { Display.AppendText("Меньше 500 МГц + УВЧ\r\n"); }
            else if (bite == "10") { Display.AppendText("Bypass по входу\r\n"); }
            else if (bite == "11") { Display.AppendText("Больше 3000 МГц + УВЧ\r\n"); }
            bite = binary.Substring(7, 1);
            if (bite == "1") { Display.AppendText($"Предусилитель в 1-м канале включен\r\n"); }
            else { Display.AppendText($"Предусилитель в 1-м канале выключен\r\n"); }
        }

        private void SetFiltersSecondCanale(string state)
        {
            string binary = String.Join(String.Empty, state.Select
                (c => Convert.ToString(Convert.ToInt32(c.ToString(), 16), 2).PadLeft(4, '0')));
            string bite = binary.Substring(0, 3);
            Display.AppendText($"Фильтр LO 2-го канала:\r\n");
            if (bite == "000") { Display.AppendText("Дипазон 860-1500 МГц\r\n"); }
            else if (bite == "001") { Display.AppendText("Дипазон 470-880 МГц\r\n"); }
            else if (bite == "010") { Display.AppendText("Дипазон 292-490 МГц\r\n"); }
            else if (bite == "011") { Display.AppendText("Дипазон 186-340 МГц\r\n"); }
            else if (bite == "100") { Display.AppendText("Дипазон 120-210 МГц\r\n"); }
            else if (bite == "101") { Display.AppendText("Дипазон 75-131 МГц\r\n"); }
            else if (bite == "110") { Display.AppendText("Дипазон 60-90 МГц\r\n"); }
            else if (bite == "111") { Display.AppendText("Дипазон 30-70 МГц\r\n"); }
            bite = binary.Substring(3, 2);
            Display.AppendText($"Фильтр HI 2-го канала:\r\n");
            if (bite == "00") { Display.AppendText("Дипазон 1500-3000 МГц\r\n"); }
            else if (bite == "01") { Display.AppendText("Дипазон 3000-4000 МГц\r\n"); }
            else if (bite == "10") { Display.AppendText("Дипазон 4000-5000 МГц\r\n"); }
            else if (bite == "11") { Display.AppendText("Дипазон 5000-6000 МГц\r\n"); }
            bite = binary.Substring(5, 2);
            Display.AppendText($"Фильтр предусилителя 2-го канала:\r\n");
            if (bite == "00") { Display.AppendText("500-3000 МГц + УВЧ\r\n"); }
            else if (bite == "01") { Display.AppendText("Меньше 500 МГц + УВЧ\r\n"); }
            else if (bite == "10") { Display.AppendText("Bypass по входу\r\n"); }
            else if (bite == "11") { Display.AppendText("Больше 3000 МГц + УВЧ\r\n"); }
            bite = binary.Substring(7, 1);
            if (bite == "1") { Display.AppendText($"Предусилитель в 2-м канале включен\r\n"); }
            else { Display.AppendText($"Предусилитель в 2-м канале выключен\r\n"); }
        }

        private void SetPreamp(string state)
        {
            string binary = String.Join(String.Empty, state.Select
                (c => Convert.ToString(Convert.ToInt32(c.ToString(), 16), 2).PadLeft(4, '0')));
            string bite = binary.Substring(0, 1);
            if (bite == "0") { Display.AppendText("Тракт HI 1-го канала\r\n"); }
            else { Display.AppendText("Тракт LO 1-го канала\r\n"); }
            bite = binary.Substring(1, 3);
            if (bite == "000") { Display.AppendText("Предусилитель выключен\r\n"); }
            else if (bite == "001") { Display.AppendText("Предусилитель 30_500M\r\n"); }
            else if (bite == "010") { Display.AppendText("Предусилитель 500M_3G\r\n"); }
            else if (bite == "011") { Display.AppendText("Предусилитель 3G_6G\r\n"); }
            bite = binary.Substring(4, 1);
            if (bite == "0") { Display.AppendText("Тракт HI 1-го канала\r\n"); }
            else { Display.AppendText("Тракт LO 1-го канала\r\n"); }
            bite = binary.Substring(5, 3);
            if (bite == "000") { Display.AppendText("Предусилитель выключен\r\n"); }
            else if (bite == "001") { Display.AppendText("Предусилитель 30_500M\r\n"); }
            else if (bite == "010") { Display.AppendText("Предусилитель 500M_3G\r\n"); }
            else if (bite == "011") { Display.AppendText("Предусилитель 3G_6G\r\n"); }
        }

        private void SwitchState(string state)
        {
            string binary = String.Join(String.Empty, state.Select
                (c => Convert.ToString(Convert.ToInt32(c.ToString(), 16), 2).PadLeft(4, '0')));
            string bite = binary.Substring(0, 3);
            if (bite == "001") { Display.AppendText("Состояние внешнего куммутатора 1 CH1\r\n"); }
            else if (bite == "010") { Display.AppendText("Состояние внешнего куммутатора 1 CH1\r\n"); }
            else if (bite == "011") { Display.AppendText("Состояние внешнего куммутатора 1 CH2\r\n"); }
            else if (bite == "100") { Display.AppendText("Состояние внешнего куммутатора 1 CH3\r\n"); }
            else if (bite == "101") { Display.AppendText("Состояние внешнего куммутатора 1 CH4\r\n"); }
            else if (bite == "111") { Display.AppendText("Коммутатор 1 выключен\r\n"); }
            bite = binary.Substring(3, 3);
            if (bite == "001") { Display.AppendText("Состояние внешнего куммутатора 2 CH1\r\n"); }
            else if (bite == "010") { Display.AppendText("Состояние внешнего куммутатора 2 CH1\r\n"); }
            else if (bite == "011") { Display.AppendText("Состояние внешнего куммутатора 2 CH2\r\n"); }
            else if (bite == "100") { Display.AppendText("Состояние внешнего куммутатора 2 CH3\r\n"); }
            else if (bite == "101") { Display.AppendText("Состояние внешнего куммутатора 2 CH4\r\n"); }
            else if (bite == "111") { Display.AppendText("Коммутатор 2 выключен\r\n"); }
        }
        private void allDecryption(string message1)
        {
            string message = "";
            Dispatcher.Invoke(() => { message = message1; });
            str = message.Substring(0, 2);
            if (str == "00") { Display.AppendText("Cинхронное управление двумя модулями преселектора\r\n"); }
            else if (str == "01") { Display.AppendText("1-й модуль преселектора\r\n"); }
            else if (str == "02") { Display.AppendText("2-й модуль преселектора\r\n"); }
            str = message.Substring(3, 2);
            if (str == "00")
            {
                string state = message.Substring(6, 2);
                int decValue = Convert.ToInt32(state, 16);
                Display.AppendText($"Значение аттенюатора LO 1-го канала: " + decValue + " дБ\r\n");
                state = message.Substring(9, 2);
                decValue = Convert.ToInt32(state, 16);
                Display.AppendText($"Значение аттенюатора HI 1-го канала: " + decValue + " дБ\r\n");
                state = message.Substring(12, 2);
                decValue = Convert.ToInt32(state, 16);
                Display.AppendText($"Значение аттенюатора предусилителя 1-го канала: " + decValue + " дБ\r\n");
                state = message.Substring(15, 2);
                SetFiltersFirstCanale(state);
                state = message.Substring(18, 2);
                SetPreamp(state);
                state = message.Substring(21, 2);
                decValue = Convert.ToInt32(state, 16);
                Display.AppendText($"Значение аттенюатора LO 2-го канала " + decValue + " дБ\r\n");
                state = message.Substring(24, 2);
                decValue = Convert.ToInt32(state, 16);
                Display.AppendText($"Значение аттенюатора HI 2-го канала " + decValue + " дБ\r\n");
                state = message.Substring(27, 2);
                decValue = Convert.ToInt32(state, 16);
                Display.AppendText($"Значение аттенюатора предусилителя 2-го канала " + decValue + " дБ\r\n");
                state = message.Substring(30, 2);
                SetFiltersSecondCanale(state);
                state = message.Substring(33, 2);
                SwitchState(state);
            }
            else if (str == "01")
            {
                Display.AppendText($"Частота: {sendFreq} МГц\r\n");
                Display.AppendText($"Младший байт частоты: {LowByteFreq} \r\n");
                Display.AppendText($"Cтарший байт частоты: {HighByteFreq}\r\n");
            }
            else if (str == "02") { Display.AppendText("Значение усиления: " + GainBox.Text + "\r\n"); }
            else if (str == "03") { Display.AppendText("Режим работы предусилителя: " + PreampBox.Text + "\r\n"); }
            else if (str == "04") { Display.AppendText("Предусилитель: " + OnOffPreamp.Text + "\r\n"); }
            else if (str == "05") { Display.AppendText("Настройка преселектора по номеру диапазона: " + PreselectorSetting.Text + "\r\n"); }
            else if (str == "06")
            {
                Display.AppendText($"Частота: {sendFreq} МГц\r\n");
                Display.AppendText($"Младший байт частоты: {LowByteFreq} \r\n");
                Display.AppendText($"Cтарший байт частоты: {HighByteFreq}\r\n");
                Display.AppendText($"Усиление: {GainBox1.Text}\r\n");
            }
            else if (str == "07") { Display.AppendText($"Режим работы внешних коммутаторов ZSWA4-63DRB+: {RangeBox.Text}\r\n"); }
            else if (str == "08") { Display.AppendText($"Управление системой коммутации с внешними коммутаторами ZSWA4-63DRB+: {LinkBox.Text}\r\n"); }
            else if (str == "09") { Display.AppendText($"Уровень аттенюатора предусилителя: {GainBox2.Text}\r\n"); }
            else if (str == "0A") { Display.AppendText($"Уровень аттенюатора в нижнем диапазоне: {GainBox3.Text}\r\n"); }
            else if (str == "0B") { Display.AppendText($"Уровень аттенюатора в верхнем диапазоне: {GainBox4.Text}\r\n"); }
            else if (str == "0F") { Display.AppendText($"Питание для внешних коммутаторов ZSWA4-63DRB+: {OnOffPreamp1.Text}\r\n"); }
            else if (str == "10") { Display.AppendText($"Питание для оптического передатчика: {OnOffPreamp2.Text}\r\n"); }
            else if (str == "12")
            {
                if (Box1.Text == Box3.Text || Box3.Text == Box5.Text || Box5.Text == Box7.Text || Box2.Text == Box4.Text || Box4.Text == Box6.Text || Box6.Text == Box8.Text)
                { Display.AppendText($"Конфигурирация каналов для системы коммутации на внешних коммутаторах ZSWA4-63DRB+: Канал для первого коммутатора/для второго коммутатора 41 32 23 14 \r\n"); }
                else
                {
                    Display.AppendText($"Конфигурирация каналов для системы коммутации на внешних коммутаторах ZSWA4-63DRB+: Канал для первого коммутатора/для второго коммутатора {Box1.Text}-{Box2.Text} {Box3.Text}-{Box4.Text} {Box5.Text}-{Box6.Text} {Box7.Text}-{Box8.Text} \r\n");
                }
            }
            else if (str == "11") { Display.AppendText($"Режим работы аттенюатора в канале предусилителя: {RangeBox1.Text}\r\n"); }
            else if (str == "13") { Display.AppendText("Параметры текущей полосы сохранены\r\n"); }
            else if (str == "14") { Display.AppendText("Произведен сброс настроек\r\n"); }
            Display.ScrollToEnd();
        }

        private void SetTract_Click(object sender, RoutedEventArgs e)
        {
            int DeviceCount = Convert.ToInt32(DeviceAddressBox.SelectedIndex);
            string[] DeviceMas = AddValueDeviceAddressCombo();
            string DeviceAddress = DeviceMas[DeviceCount];
            p.SetPathGainCode15((byte)Convert.ToInt16(TracktBox.Text), (PreselectorNumber)Convert.ToByte(DeviceAddress));
        }

        private void TypeConnectionComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            typeConnection = Convert.ToBoolean(TypeConnectionComboBox.SelectedIndex);
        }

        private void ConnectTCPButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.preselTcp = new PreselTcpOld(IpBox.Text, Convert.ToInt16(PortBox.Text));
                this.p = this.preselTcp as PreselModelOld;
                var a = p.Connect();
                if (a == true)
                {
                    CheckEl.Fill = new SolidColorBrush(Colors.PaleGreen);
                    this.preselTcp.OnRead += Client_DataReceived;
                    this.preselTcp.OnWrite += P_OnWriteBTcp;
                }
            }
            catch
            {

            }
        }

        private void P_OnWriteBTcp(object sender, string e)
        {
            try
            {
                string b = string.Empty;
                foreach (var a in e)
                {
                    b += a.ToString();
                }
                b = AddSpaces(b);
                this.Dispatcher.Invoke(() => Display.AppendText(b + "\r\n"));
            }
            catch { }
        }

        private void Client_DataReceived(object sender, string e)
        {
            try
            {
                string b = string.Empty;
                //foreach (var a in e)
                //{
                //    b += a.ToString();
                //}
                b = AddSpaces(e);

                this.Dispatcher.Invoke(() => Display.AppendText(b + "\r\n"));
            }
            catch { }
        }

        private void DisconnectTcpButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                p.Disconnect();
                CheckEl.Fill = new SolidColorBrush(Colors.Red);
            }
            catch
            {

            }
        }
    }
}

